trdnsrank
================

A benchmark for querying domain name servers (DNS) written in Racket.

![example](https://abload.de/img/s2ir0q.png)

Usage
----------------

Run `racket main.rkt --help` to see a help message.

The program expects the following input:  
`<list of server ips> <list of domain names>`  
where the lists elements are strings.

An input example:  
`("1.1.1.1" "8.8.8.8") ("google.com" "bitbucket.org")`

Run the example with  
`racket main.rkt < input_example.txt`  
or  
`cat input_example.txt | racket main.rkt`

The program supports multiple kind of outputs:

* table (`--table`)
* unicode table (`--unicode` *default*)
* s-expression (`--sexp`)
* csv (`--csv`)

Dependencies
----------------

* [bitsyntax](https://pkgs.racket-lang.org/package/bitsyntax)
* [text-table](https://pkgs.racket-lang.org/package/text-table)

Install with `raco pkg install bitsyntax text-table`.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

