#|
    trdnsrank

    Contributors:
         Tetsumi <tetsumi@protonmail.com>

    Copyright (C) 2017 Tetsumi
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
|#

#lang racket

(require bitsyntax
         text-table)

(define udp (udp-open-socket))
(udp-bind! udp #f 0)

(define (timeResolve server name)
  (define id (random 65536))
  (define qname (for/fold ([bs #""])
                          ([label (string-split name ".")])
                  (let ([lbs (string->bytes/utf-8 label)])
                    (bytes-append bs (bytes (bytes-length lbs)) lbs))))
  #|
    The query header (RFC1035 4.1.1):
                                      1  1  1  1  1  1
        0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                      ID                       |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                    QDCOUNT                    |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                    ANCOUNT                    |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                    NSCOUNT                    |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                    ARCOUNT                    |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

    The query body (RFC1035 4.1.2):
                                      1  1  1  1  1  1
        0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                                               |
      /                     QNAME                     /
      /                                               /
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                     QTYPE                     |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
      |                     QCLASS                    |
      +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  |#
  (define packet (bit-string [    id :: bits 16] ; ID
                             [     0 :: bits  1] ; QR
                             [     0 :: bits  4] ; Opcode
                             [     0 :: bits  1] ; AA
                             [     0 :: bits  1] ; TC
                             [     1 :: bits  1] ; RD
                             [     0 :: bits  1] ; RA
                             [     0 :: bits  3] ; Z
                             [     0 :: bits  4] ; RCODE
                             [     1 :: bits 16] ; QDCOUNT
                             [     0 :: bits 16] ; ANCOUNT
                             [     0 :: bits 16] ; NSCOUNT
                             [     0 :: bits 16] ; ARCOUNT
                             [ qname :: binary ] ; QNAME
                             [     0 :: bits  8] ; QNAME END MARK
                             [     1 :: bits 16] ; QTYPE
                             [     1 :: bits 16] ; QCLASS
                             ))
  (define buffer (make-bytes 512))
  (define tBefore (current-inexact-milliseconds))
  (udp-send-to udp server 53 (bit-string->bytes packet))
  (let loop ()
    (if (sync/timeout 1.0 (udp-receive!-evt udp buffer))
        (let ([tAfter (current-inexact-milliseconds)])
          (bit-string-case buffer
            ([(= id :: bits 16)
              (:: bits 12)
              (= 0 :: bits 4)
              (rest :: binary)]
             (abs (- tAfter tBefore)))
            (else (loop))))
        #f)))

(define (timeResolveF server name)
  (let ([result (timeResolve server name)])
    (if result
        (~r result #:precision 2 #:min-width 6)
        "timeout")))

(define outputFormat (make-parameter 'unicode))

(command-line
 #:program "trdnsrank"
 #:usage-help
 "Input shall be a list of server IPs followed by a list of URL."
 "   example: (\"1.1.1.1\" \"2.2.2.2\") (\"www.google.com\")"
 #:once-any
 ["--csv" "format ouput as CSV." (outputFormat 'csv)]
 ["--unicode" "format output as unicode table." (outputFormat 'unicode)]
 ["--sexp" "format output as s-expressions." (outputFormat 'sexp)]
 ["--table" "format ouput as table" (outputFormat 'table)])

(define servers (read))
(define names (read))
(define results (cons (cons "Server" names)
                      (for/list ([server servers])
                        (cons server
                              (map (curry timeResolveF server)
                                   names)))))

(define (printResults formatStr)
  (for ([row results])
    (for ([column row])
      (printf formatStr column))
    (newline)))

(case (outputFormat)
   ['unicode (displayln (table->string results #:align 'right))]
   ['sexp    (displayln results)]
   ['table   (printResults "~a ")]
   ['csv     (printResults "~v, ")]
   [else (error "unknown output format")])

(udp-close udp)
